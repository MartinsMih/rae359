#rae359#
##Distributed systems##
###About:###
This is an example of distributed system, where Spring Boot rest Service is handling POST GET PUT requests from client, which is a simple android application.
The service was created with IntelliJ IDEA, but the client was createn with Android Studio.
System covers the initial task to handle POST message: {"id":"1","name":"somePersonName","title":"someTitle","content":"someContent"}
###How to use restClient###
Before using restClient you have to start restService in the same network in which your restClient will be deployed.
In the url field of rest client use your servers IP address. For more info watch the video preview:
[How to use restClient](https://youtu.be/ezCKtNs09wQ)
###More about distributed systems###
I made this video to give a simple explanation on what distributed systems are and which are core Apache software solutions that are handling them.
To make it more understandably I added a narration by Google AI:
[What's distributed system](https://youtu.be/Q1IZ-KelUVo)