package com.example.demo;

import com.example.demo.message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
public class messageController {
    private static final Logger logger = LoggerFactory.getLogger(messageController.class);

    Map<Integer, message> msgData = new HashMap<Integer, message>();

    @RequestMapping(value = messageURIConstants.DUMMY_MSG, method = RequestMethod.GET)
    public @ResponseBody
    message getDummyMessage() {
        logger.info("Start getDummyMessage");
        message msg = new message();
        msg.setId(10000);
        msg.setName("Dummy");
        msg.setTitle("checkMessage");
        msg.setContent("some info");
        msgData.put(9999, msg);
        return msg;
    }

    @RequestMapping(value = messageURIConstants.GET_MSG, method = RequestMethod.GET)
    public @ResponseBody message getMessage(@PathVariable("id") int msgId) {
        logger.info("Start getMessage. ID="+msgId);

        return msgData.get(msgId);
    }
    @RequestMapping(value = messageURIConstants.GET_ALL_MSG, method = RequestMethod.GET)
    public @ResponseBody
    List<message> getAllEmployees() {
        logger.info("Start getAllMessages.");
        List<message> msgs = new ArrayList<message>();
        Set<Integer> empIdKeys = msgData.keySet();
        for(Integer i : empIdKeys){
            msgs.add(msgData.get(i));
        }
        return msgs;
    }
    @RequestMapping(value = messageURIConstants.CREATE_MSG, method = RequestMethod.POST)
    public @ResponseBody message createEmployee(@RequestBody message msg) {
        logger.info("Start createMessage.");
        msgData.put(msg.getId(), msg);
        return msg;
    }
    @RequestMapping(value = messageURIConstants.DELETE_MSG, method = RequestMethod.PUT)
    public @ResponseBody message deleteEmployee(@PathVariable("id") int empId) {
        logger.info("Start deleteEmployee.");
        message msg = msgData.get(empId);
        msgData.remove(empId);
        return msg;
    }
}
