package com.mihaeljans.martins.restclient;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;

public class RestClient extends AppCompatActivity {
    int id;
    String URL;
    String name;
    String title;
    String content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_client);
        Button get =(Button) findViewById(R.id.get);
        get.setOnClickListener(button_click);
        Button put =(Button) findViewById(R.id.put);
        put.setOnClickListener(button_click);
        Button post =(Button) findViewById(R.id.post);
        post.setOnClickListener(button_click);
    }
    View.OnClickListener button_click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.get:
                    get();
                    break;
                case R.id.post:
                    post();
                    break;
                case R.id.put:
                    put();
                    break;
            }
        }
    };
    public void get(){
        EditText urlt = (EditText)findViewById(R.id.url);
        URL=urlt.getText().toString();
        final TextView responseT = (TextView)findViewById(R.id.response);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        responseT.setText("Response is: "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseT.setText("That didn't work!");
            }
        });
        queue.add(stringRequest);
    }
    public void post(){
        EditText urlt = (EditText)findViewById(R.id.url);
        EditText idi = (EditText)findViewById(R.id.id);
        EditText namet = (EditText)findViewById(R.id.name);
        EditText titlet = (EditText)findViewById(R.id.title);
        EditText contentt = (EditText)findViewById(R.id.content);
        final TextView responseT = (TextView)findViewById(R.id.response);
        URL=urlt.getText().toString();
        id=Integer.valueOf(idi.getText().toString());
        name=namet.getText().toString();
        title=titlet.getText().toString();
        content=contentt.getText().toString();
        try {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("id",id);
        jsonBody.put("name",name);
        jsonBody.put("title",title);
        jsonBody.put("content",content);
        final String mRequestBody = jsonBody.toString();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("LOG_VOLLEY", response);
                //String result = response.toString();
                responseT.setText("Message has been created use GET on /rest/msg/"+id+" to view it");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("LOG_VOLLEY", error.toString());
                String result = error.toString();
                responseT.setText(result);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                    return null;
                }
            }
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {

                    responseString = String.valueOf(response.statusCode);
                    String result = response.toString();
                    responseT.setText(result);

                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };

        requestQueue.add(stringRequest);
    } catch (JSONException e) {
        e.printStackTrace();
    }

    }
    public void put(){
        EditText urlt = (EditText)findViewById(R.id.url);
        URL=urlt.getText().toString();
        final TextView responseT = (TextView)findViewById(R.id.response);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        responseT.setText("Response is: "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseT.setText("That didn't work!");
            }
        });
        queue.add(stringRequest);
    }
}
