package com.example.demo;

public class messageURIConstants {
    public static final String DUMMY_MSG = "/rest/msg/dummy";
    public static final String GET_MSG = "/rest/msg/{id}";
    public static final String GET_ALL_MSG = "/rest/msgs";
    public static final String CREATE_MSG = "/rest/msg/create";
    public static final String DELETE_MSG = "/rest/msg/delete/{id}";
}
